# readme

getting started ansible

`pip install -r requirements.txt`

`ansible-galaxy install  -r ansible-galaxy-requirements.yml `

## raspberry pi

### preparation

* add user raoel and give sudo
* ssh-copy-id raoel
* apt update && apt upgrade
* change hostname
* reboot

## TODO

* ntp hosts aan /etc/hosts toevoegen:

```195.3.254.2:123 0.europe.pool.ntp.org
213.136.0.252  1.pool.ntp.org
```

* ssh config aanpassen (lineinfile enzo)

* wifi uitzetten (met flag = wifi_disabled)
* openvpn vanaf eris
* tinc of wireguard voor cluster
* monitoring op cluster ()
* prometheus integration
* ~~node_exporter  installation and configuration~~
